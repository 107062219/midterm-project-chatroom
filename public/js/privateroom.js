var now_chatwith = undefined;
var now_user = undefined;
var now_time = undefined;
var name = undefined;
var name2 = undefined;
var isOnline;
var name_now = undefined;
function init() {
    var user_email = '';
    //var user_email2 = '';
    var user_uid = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            now_user = user.email;
            user_email = user.email;
            user_uid = user.uid;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            //get name for now_user
            var flag=0;
            for(let j in now_user){
                if(now_user[j] == '@'){
                    flag=j;
                }
            }
            name = now_user.substring(0,flag);
            //logout
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    var newer_logout={
                        online: false
                    }
                    firebase.database().ref('online_list/' + name + '/').set(newer_logout);
                    alert('logout success');
                    var user = firebase.auth().currentUser;
                    console.log(user);
                    window.location.assign("signin.html");
                })
                .catch(function(error) {
                    // Handle Errors here.
                    alert('logout fail');
                })
            });

            //show user
            var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/icon.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div>";
            var str_end = "</div>\n";
            var loginRef = firebase.database().ref('user_list');
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            loginRef.once('value')
            .then(function(snapshot) {
                //show current users
                document.getElementById('information').innerHTML = total_post.join('');
                loginRef.on('child_added', function(data) {
                    var flag3=0;
                    for(let k in data.val().email){
                        if(data.val().email[k] == '@'){
                            flag3=k;
                        }
                    }
                    name_now = data.val().email.substring(0,flag3);
                    //button
                    var str_btn = "<button class='btn' style='background-color: blue;' onclick='choose(this)' value='"+ data.val().email+"'>" + "</button>";
                    //button with unread signal
                    var str_btn2 = "<button class='btn' style='background-color: red;' onclick='choose(this)' id='"+ name_now+"' value='"+ data.val().email+"'>" + "</button>";
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData = data.val();
                        //only show other, not self
                        if(childData.email != user.email){
                            var isread;
                            var readRef = firebase.database().ref('read_list/' + name + '/' + name_now + '/');
                            readRef.once('value').then(function(snapshot){
                                var x = snapshot.val();
                                isread = x.isRead;
                                console.log(isread);
                                console.log(typeof isread);
                            }).then(function(){
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_after_content + str_btn + str_end
                                document.getElementById('information').innerHTML = total_post.join('');
                            }).catch(function(){
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_after_content + str_btn2 + str_end
                                document.getElementById('information').innerHTML = total_post.join('');
                            })
                            /*if(flag_btn==1){
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_after_content + str_btn2 + str_end
                            }
                            else{
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_after_content + str_btn + str_end
                            }*/
                            
                        }
                    }
                });
            })
            .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            alert("You should login first! Or you can't chat with anyone!")
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    

    /*upload new message*/
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            now_time=getTime();
            post_txt.value = escapeHtml(post_txt.value);
            var newer={
                "email": user_email,
                "data" : post_txt.value,
                "time" : now_time
            }
            firebase.database().ref('history_list/' + name + '/' + name2 + '/').push(newer);
            firebase.database().ref('history_list/' + name2 + '/' + name + '/').push(newer);
            post_txt.value = "";

            //check if needed to change isConfirm
            firebase.database().ref('online_list/' + name2 + '/').once('value').then(function(snapshot){
                isOnline = snapshot.val().online;
                if(!isOnline){
                    console.log("!isOnline")
                    var newer_notification={
                        isConfirm : "yes"
                    }
                    firebase.database().ref('notification_list/' + name2 + '/').set(newer_notification);
                }
                firebase.database().ref('read_list/' + name2 + '/' + name + '/').remove();   
            });
            
        }
    });

}

//show history
function choose(x){
    // The html code for post
    now_chatwith = x.value;  
    //folder name
    var flag2=0;
    for(let k in now_chatwith){
        if(now_chatwith[k] == '@'){
            flag2=k;
        }
    }
    name2 = now_chatwith.substring(0,flag2);
    //
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username_tmp = "</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>"
    var str_before_username2 = "<div class='my-3 p-3 bg-white rounded box-shadow' id='my_message'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username_tmp2 = "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>"
    var str_after_content = "</p></div></div>\n";
    var str_after_content2 = "</p><img src='img/icon.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></div></div>\n";

    var postsRef = firebase.database().ref('history_list/' + name + '/' + name2 + '/');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            document.getElementById('post_list').innerHTML = total_post.join('');
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.email != now_user){
                        total_post[total_post.length] = str_before_username + childData.time + str_before_username_tmp + childData.email + "</strong>" + childData.data + str_after_content
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    else{
                        total_post[total_post.length] = str_before_username2 + childData.time + str_before_username_tmp2 + childData.email + "</strong>" + childData.data + str_after_content2
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                }
            });


        })
        .catch(e => console.log(e.message));

    //set btn-color blue
    var newer={
        isRead : "yes"
    }
    firebase.database().ref('read_list/' + name + '/' + name2 + '/').set(newer);
    //x.style.color = blue;
    document.getElementById(name2).style.backgroundColor = "blue";
}
function escapeHtml(str)  
{  
    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/"/g, '&quot;');
    str = str.replace(/'/g, '&#039;');
    return str;
}
function getTime() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    if (h < 10) {
      h = '0' + h;
    }
    if (m < 10) {
      m = '0' + m;
    }
    if (s < 10) {
      s = '0' + s;
    }
    var now = month + '/' + day + ' ' + h + ':' + m + ':' + s;
    return now;
}

window.onload = function() {
    init();
};