function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('loginEmail');
    var txtPassword = document.getElementById('loginPass');
    var signupEmail = document.getElementById('signupEmail');
    var signupPassword = document.getElementById('signupPass');
    var signupPassword2 = document.getElementById('signupPass2');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
            window.location.assign("index.html");
        })
        .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            //create_alert("error","fail");
            alert("signIn error!")
            txtEmail.value = '';
            txtPassword.value = '';
        });
              
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.  var user = result.user;
            // ...
            window.location.assign("index.html");
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.  var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
        });
            

    });

    btnSignUp.addEventListener('click', function() {
        var email = signupEmail.value;
        var password = signupPassword.value;
        var password2 = signupPassword2.value;
        if(password!=password2){
            alert("Password isn't the same!");
            signupEmail.value = '';
            signupPassword.value = '';
        }
        else{
            firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
                //create_alert("success","signup success!");
                alert("signup Success!Please login to show you to others!")
                //upload user
                signupEmail.value = '';
                signupPassword.value = '';
                signupPassword2.value = '';
                //reset isConfirm
                /*var name;
                var flag=0;
                for(let j in email){
                    if(email[j] == '@'){
                        flag=j;
                    }
                }
                name = email.substring(0,flag);
                var newer_notification={
                    isConfirm : "no"
                }
                firebase.database().ref('notification_list/' + name + '/').set(newer_notification);*/
            })
            .catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                //create_alert("error","fail!");
                alert("signup Error!")
                signupEmail.value = '';
                signupPassword.value = '';
                signupPassword2.value = '';
            });
        }
    });
}

// Custom alert
/*function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}*/

window.onload = function() {
    initApp();
};