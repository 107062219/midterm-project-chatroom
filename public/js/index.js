var now_user = undefined;
var name = undefined;
var now_time = undefined;
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            now_user = user.email;
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            //get name for now_user,turn online=1
            var flag=0;
            for(let j in now_user){
                if(now_user[j] == '@'){
                    flag=j;
                }
            }
            name = now_user.substring(0,flag);
            var newer_logout={
                online: true
            }
            firebase.database().ref('online_list/' + name + '/').set(newer_logout);
            
            //check if needed to notification
            var isconfirm;
            firebase.database().ref('notification_list/' + name + '/').once('value').then(function(snapshot){
                isconfirm = snapshot.val().isConfirm;
                console.log(isconfirm);

                if(isconfirm){
                    console.log("unconfirm=0")
                    notification();
                    //notify once
                    firebase.database().ref('notification_list/' + name + '/').remove();
                }
            });
            


            //logout
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    var newer_logout={
                        online: false
                    }
                    firebase.database().ref('online_list/' + name + '/').set(newer_logout);
                    alert('logout success');
                    var user = firebase.auth().currentUser;
                    console.log(user);
                    window.location.assign("signin.html");
                })
                .catch(function(error) {
                    // Handle Errors here.
                    alert('logout fail');
                })
            });

            //loadup user
            var loginRef = firebase.database().ref('user_list');
            var flag=0;
            var flag2=0;
            loginRef.once('value')
            .then(function(snapshot) {
                for(let i in snapshot.val()){
                    if(snapshot.val()[i].email == user_email){
                        flag = 1;
                    }
                }

                //
                if(flag!=1){
                    firebase.database().ref('user_list/').push({
                        "email": user_email
                    });
                }

            })
            .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            alert("You should login first! Or you can't post anything!");
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            now_time=getTime();
            var user = firebase.auth().currentUser;
            post_txt.value = escapeHtml(post_txt.value);
            firebase.database().ref('com_list/').push({
                "email": user.email,
                "data" : post_txt.value,
                "time" : now_time
            });
            post_txt.value = "";
                
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow' style='background-image: linear-gradient(to right, #ec77ab 0%, #7873f5 100%);'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username_tmp = "</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray text-white-50'><strong class='d-block'>"
    var str_before_username2 = "<div class='my-3 p-3 bg-white rounded box-shadow' style='background-image: linear-gradient(-225deg, #5271C4 0%, #B19FFF 48%, #ECA1FE 100%);'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username_tmp2 = "</h6><div class='media text-muted pt-3'><img src='img/icon.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray text-white-50'><strong class='d-block'>"
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');
            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.email != now_user){
                        total_post[total_post.length] = str_before_username2 + childData.time + str_before_username_tmp + childData.email + "</strong>" + childData.data + str_after_content
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    else{
                        total_post[total_post.length] = str_before_username + childData.time + str_before_username_tmp2 + "my message" + "</strong>" + childData.data + str_after_content
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                }
            });


        })
        .catch(e => console.log(e.message));
}

function escapeHtml(str)  
{  
    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/"/g, '&quot;');
    str = str.replace(/'/g, '&#039;');
    return str;
}
function notification(){
    if (Notification.permission == "granted") {
        var notification = new Notification(name, {
            body: 'you got new message unread!'
        });
    }
    else{
        console.log("555");
        Notification.requestPermission().then(function(permission){   
            console.log("999");
            var popNotice = function() {
                var notification = new Notification("Hi：", {
                    body: 'you got new message unread!'
                });
            };
            if (Notification.permission == "granted") {
                popNotice();
                console.log("666");    
            } else if (Notification.permission != "denied") {
                Notification.requestPermission(function (permission) {
                  popNotice();
                });
                console.log("777");    
            }else{
                alert("Check if your browser support notification!");
                console.log("888");    
            }

        })
    }
}
function getTime() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    if (h < 10) {
      h = '0' + h;
    }
    if (m < 10) {
      m = '0' + m;
    }
    if (s < 10) {
      s = '0' + s;
    }
    var now = month + '/' + day + ' ' + h + ':' + m + ':' + s;
    return now;
}

window.onload = function() {
    init();
};