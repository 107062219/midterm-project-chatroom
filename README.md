# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 
    * gitlab : midterm project - chatroom
    * firebase : chatroom
* Key functions (add/delete)
    1. Sign Up/In
    2. Chatroom
    
* Other functions (add/delete)
    1. Sign in with Google
    2. Chrome Notification 
    3. CSS Animation 
    4. Lobby
    5. Unread message
    6. RWD

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://chatroom-82a94.web.app/

# Components Description : 
1. Sign Up/In : 登入/註冊
*  #### 功能: 
    在SignIn畫面可以用已經註冊好的帳號登入，或是用Google帳號登入；如果尚未註冊過的話可點擊SIGN UP，畫面即會旋轉至背面並呈現註冊畫面，註冊時需要輸入兩次密碼，如果兩次密碼不同會跳出警告"Password isn't the same!"；成功登入後會跳轉至大廳(公共聊天室)
    ![](https://i.imgur.com/omUznIH.jpg)

2. Chatroom : 私人聊天室
*  #### 功能:
    * #### Create a private chatroom / Load all history message of current chatroom : 
        進入私人聊天室後會有下面畫面，左邊會有現在可聊天的對象清單，點擊頭貼下的按鈕就可以載入之前與此人的對話紀錄，並且跟選擇的用戶聊天
    ![](https://i.imgur.com/OmXu31H.jpg)
    * #### Send messages in chatroom : 
        在New Post的欄位裡打入想送出的訊息再按下送出即可傳送訊息，如果是自己的訊息顏色會是彩色，如果是別人的訊息會是白色
    ![](https://i.imgur.com/7upwpPV.jpg)
    

# Other Functions Description : 
1. Sign in with Google : 可使用Google帳號登入
    ##### 點擊下方的按鈕列可選擇目前繪畫模式在登入介面可直接點選下方Sign in with Google的按鈕，即可選擇使用Google帳號登入
    
2. Chrome Notification  : Chrome提醒
    ##### 如果現在傳送訊息的對象不在線上的話，仍可以傳訊息給他，而在對方下次登入時會出現chrome提醒，當收到提醒時表示在使用者不在線的有人傳訊息給自己(只會提醒一次，意即假如這次登入收到提醒但還是沒去讀未讀的訊息，下次進來就不會再提醒，除非在離線期間又收到新的訊息)
    ![](https://i.imgur.com/8ILcJAt.jpg)

3. CSS Animation  : CSS動畫
    ##### (1)在登入畫面，點擊SIGN UP會旋轉到背面呈現註冊畫面
    ##### (2)在大廳的風車跟漂浮雲朵

4. Lobby  : 公共聊天室
    ##### 一開始登入後會跳到公共聊天室，點擊上方的navber的private中的room即可跳到私人聊天室，在私人聊天室可藉由同樣方式回到公共聊天室；公共聊天室可以被所有人共同使用，類似論壇的感覺因此稱為Post/Submit，如果是自己傳送的訊息會是紫色且標示"my message"，如果是別人的訊息會是藍色，上方會顯示對方的email；如果沒有登入的話，只能讀公共訊息，但不能Post訊息
    ![](https://i.imgur.com/RCvHlK4.jpg)
 
5. Unread message  : 未讀訊息
    ##### 進入私人聊天室後如果在離線期間有收到訊息，且仍未閱讀的話在該對話人下方的按鈕會呈現紅色，點擊按鈕進入兩人的聊天室後按鈕會變回藍色；如果沒有未讀的訊息或是不是在離線時收到的訊息，按鈕則呈現藍色(一開始都還沒開始對話時呈現紅色，類似line的新好友的對話欄都是先設為未讀
    ![](https://i.imgur.com/9wrHE5a.jpg)

6. RWD  : 
    ##### (1)在lobby的時候，如果畫面寬度小於995px，原本是左邊風車跟右邊論壇變成上方風車下方論壇；如果畫面寬度小於767.98px，則上方的navbar摺疊為一個按鈕下的選單
    ![](https://i.imgur.com/AdAM4xr.jpg)
    ##### (2)在private的時候如果畫面寬度小於767.98px，則上方的navbar摺疊為一個按鈕下的選單，原本是左邊user_list跟右邊對話紀錄變成上方user_list下方對話紀錄
    ![](https://i.imgur.com/Q4n24ku.jpg)


## Security Report (Optional)
*  #### Deal with HTML code  : 
    ##### 在Post的欄位輸入HTML的格式，例如:div,br也不會改到HTML的格式，會被當成普通訊息傳出
    ![](https://i.imgur.com/S43tcbX.jpg)